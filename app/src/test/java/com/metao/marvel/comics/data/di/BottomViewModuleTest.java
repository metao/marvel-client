package com.metao.marvel.comics.data.di;

import com.metao.marvel.data.di.ScreenScoped;
import com.metao.marvel.presentation.bottomview.BottomViewContract;

import dagger.Module;
import dagger.Provides;

/**
 * Comic list module for test.
 */
@Module
public class BottomViewModuleTest {

    private final BottomViewContract.View view;

    public BottomViewModuleTest(BottomViewContract.View view) {
        this.view = view;
    }

    @Provides
    @ScreenScoped
    BottomViewContract.View provideView() {
        return view;
    }

}
