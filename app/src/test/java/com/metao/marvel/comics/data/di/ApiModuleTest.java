package com.metao.marvel.comics.data.di;

import com.metao.marvel.BuildConfig;
import com.metao.marvel.DefaultSchedulerProvider;
import com.metao.marvel.common.scheduler.SchedulerProvider;
import com.metao.marvel.data.di.ApiModule;
import com.metao.marvel.data.remote.ApiService;

import org.mockito.Mockito;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Api module to provide the api related objects.
 */
@Module
public class ApiModuleTest extends ApiModule{

}
