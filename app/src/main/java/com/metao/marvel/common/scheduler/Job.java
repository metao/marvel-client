package com.metao.marvel.common.scheduler;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class Job implements Parcelable {

    public static final Parcelable.Creator<Job> CREATOR = new Parcelable.Creator<Job>() {
        @Override
        public Job createFromParcel(Parcel source) {
            return new Job(source);
        }

        @Override
        public Job[] newArray(int size) {
            return new Job[size];
        }
    };
    @NonNull
    private final String jobId;
    public ConcurrentHashMap<String, String> jobHParamsHash = new ConcurrentHashMap<>();
    @NonNull
    private String type;

    public Job(@NonNull String type) {
        this.jobId = UUID.randomUUID().toString().substring(0, 4);
        this.type = type;
    }

    protected Job(Parcel in) {
        this.jobHParamsHash = (ConcurrentHashMap<String, String>) in.readSerializable();
        this.jobId = in.readString();
        this.type = in.readString();
    }

    @NonNull
    public String getType() {
        return type;
    }

    @NonNull
    public String getJobId() {
        return jobId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeSerializable(this.jobHParamsHash);
        dest.writeString(this.jobId);
        dest.writeString(this.type);
    }
}
