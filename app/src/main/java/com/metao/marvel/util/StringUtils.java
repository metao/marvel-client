package com.metao.marvel.util;


/**
 * String utilities.
 */
public class StringUtils {

    public static boolean isNullOrEmpty(String string) {
        return string == null || string.isEmpty();
    }
}
