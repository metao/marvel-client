package com.metao.marvel.presentation.base;


import android.support.v4.app.Fragment;

import com.metao.marvel.R;

public class BaseFragment extends Fragment {


    /**
     * Starts a fragment and adds it to back stack.
     *
     * @param fragment - fragment to start
     */
    public void startFragment(Fragment fragment) {
        if (getActivity() != null) {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, fragment, fragment.getClass().getSimpleName())
                    .addToBackStack(null)
                    .commit();
        }
        /*loadBottomSheet(BottomViewFragment.newInstance());*/
    }

  /*  public void loadBottomSheet(Fragment fragment) {
        if (getActivity() != null) {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.bottom_sheet_container, fragment, fragment.getClass().getSimpleName())
                    .commit();
        }
    }*/
}
