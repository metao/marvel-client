package com.metao.marvel.presentation;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;

import com.metao.marvel.R;
import com.metao.marvel.presentation.bottomview.BottomViewFragment;
import com.metao.marvel.presentation.comiclist.ComicListFragment;
import com.metao.marvel.presentation.comiclist.ItemsSwitchListener;

public class MainActivity extends AppCompatActivity implements ItemsSwitchListener {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, ComicListFragment.newInstance(),
                            ComicListFragment.class.getName())
                    .commit();
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.bottom_sheet_container, BottomViewFragment.newInstance(),
                            BottomViewFragment.class.getName())
                    .commit();

        }
    }
    //TODO change layers and fragments
    @Override
    public void onItemChanged(String itemId) {

    }
}
