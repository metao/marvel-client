package com.metao.marvel.presentation.bottomview.di;


import com.metao.marvel.data.di.ScreenScoped;
import com.metao.marvel.data.remote.ApiService;
import com.metao.marvel.presentation.bottomview.BottomViewFragment;

import dagger.Subcomponent;

/**
 * ComicList dagger sub component.
 */
@ScreenScoped
@Subcomponent(modules = BottomViewModule.class)
public interface BottomViewComponent {

    void inject(BottomViewFragment fragment);
}
