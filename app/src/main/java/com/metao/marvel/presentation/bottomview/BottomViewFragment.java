package com.metao.marvel.presentation.bottomview;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.metao.marvel.App;
import com.metao.marvel.R;
import com.metao.marvel.databinding.ComicBottomView;
import com.metao.marvel.presentation.base.BaseFragment;
import com.metao.marvel.presentation.bottomview.di.BottomViewModule;

import javax.inject.Inject;

public class BottomViewFragment extends BaseFragment implements BottomViewContract.View {

    @Inject
    BottomViewPresenter presenter;
    private ComicBottomView binding;
    private BottomNavigationView.OnNavigationItemSelectedListener myNavigationItemListener
            = item -> {
                switch (item.getItemId()) {
                    case R.id.action_like:

                        break;
                    case R.id.action_dislike:
                        break;
                    default://all items
                }
                return true;
            };

    public static Fragment newInstance() {
        return new BottomViewFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        injectComponents();
        presenter.onAttach(this);
    }

    private void initNavigationView() {
        binding.bottomNavigation.setOnNavigationItemSelectedListener(myNavigationItemListener);
        binding.bottomNavigation.setSelectedItemId(R.id.action_all);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_bottom_list, container,
                false);
        initNavigationView();
        return binding.getRoot();
    }

    private void injectComponents() {
        if (getActivity() != null) {
            ((App) getActivity().getApplication())
                    .getAppComponent()
                    .plus(new BottomViewModule(this))
                    .inject(this);
        }
    }

}
