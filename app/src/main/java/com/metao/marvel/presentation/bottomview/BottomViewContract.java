package com.metao.marvel.presentation.bottomview;

import com.metao.marvel.data.model.Comic;
import com.metao.marvel.presentation.base.BasePresenter;
import com.metao.marvel.presentation.base.BaseView;

public class BottomViewContract {

    interface Presenter extends BasePresenter<View> {

        void findComics(String budget);

        void navigateToComicDetails(Comic comic);
    }

    public interface View extends BaseView {


    }

}
