package com.metao.marvel.presentation.bottomview;

import com.metao.marvel.data.model.Comic;

import javax.inject.Inject;

public class BottomViewPresenter implements BottomViewContract.Presenter {

    @Inject
    public BottomViewPresenter() {

    }

    @Override
    public void findComics(String budget) {

    }

    @Override
    public void navigateToComicDetails(Comic comic) {

    }

    @Override
    public void onAttach(BottomViewContract.View view) {

    }

    @Override
    public void onDetach() {

    }
}
