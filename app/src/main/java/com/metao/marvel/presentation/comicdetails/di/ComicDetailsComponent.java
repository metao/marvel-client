package com.metao.marvel.presentation.comicdetails.di;


import com.metao.marvel.data.di.ScreenScoped;
import com.metao.marvel.presentation.comicdetails.ComicDetailsFragment;

import dagger.Subcomponent;

@ScreenScoped
@Subcomponent(modules = ComicDetailsPresenterModule.class)
public interface ComicDetailsComponent {

    void inject(ComicDetailsFragment fragment);

}
