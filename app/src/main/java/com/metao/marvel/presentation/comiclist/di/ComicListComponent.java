package com.metao.marvel.presentation.comiclist.di;


import com.metao.marvel.data.di.ScreenScoped;
import com.metao.marvel.presentation.comiclist.ComicListFragment;

import dagger.Subcomponent;

/**
 * ComicList dagger sub component.
 */
@ScreenScoped
@Subcomponent(modules = ComicListPresenterModule.class)
public interface ComicListComponent {

    void inject(ComicListFragment fragment);

}
