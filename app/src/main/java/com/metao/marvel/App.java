package com.metao.marvel;

import android.app.Application;

import com.metao.marvel.di.AppComponent;
import com.metao.marvel.di.AppModule;
import com.metao.marvel.di.DaggerAppComponent;
import com.metao.marvel.util.RealmHelper;


public class App extends Application {

    private static AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        buildAppComponent();

        // init realm
        RealmHelper.init(this);
    }

    private void buildAppComponent() {
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
