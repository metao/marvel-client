package com.metao.marvel.data.di;


import com.metao.marvel.BuildConfig;
import com.metao.marvel.DefaultSchedulerProvider;
import com.metao.marvel.common.scheduler.SchedulerProvider;
import com.metao.marvel.data.remote.ApiService;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Api module to provide the api related objects.
 */
@Module
public class ApiModule {

    @Singleton
    @Provides
    SchedulerProvider provideSchedulerProvider() {
        return new DefaultSchedulerProvider();
    }

    @Provides
    @Singleton
    public ApiService provideApiService(OkHttpClient okHttpClient, GsonConverterFactory gsonConverterFactory) {
        Retrofit.Builder requestBuilder = new Retrofit.Builder();
        requestBuilder
                .baseUrl(ApiService.BASE_URL)
                .addConverterFactory(gsonConverterFactory)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient);
        Retrofit retrofit = requestBuilder.build();

        return retrofit.create(ApiService.class);
    }

    @Provides
    @Singleton
    public OkHttpClient okHttpClient() {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        if (BuildConfig.DEBUG) {
            httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        } else {
            httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.NONE);
        }
        return new OkHttpClient
                .Builder()
                .connectTimeout(20000, TimeUnit.MILLISECONDS)
                .readTimeout(20000, TimeUnit.MILLISECONDS)
                .writeTimeout(20000, TimeUnit.MILLISECONDS)
                //.addInterceptor(myServiceInterceptor)
                .addInterceptor(httpLoggingInterceptor)
                .build();
    }

    @Provides
    @Singleton
    public GsonConverterFactory provideGsonConverterFactory() {
        return GsonConverterFactory.create();
    }
}
