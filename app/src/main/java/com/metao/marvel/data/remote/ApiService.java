package com.metao.marvel.data.remote;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * API service to interact with the API.
 */
public interface ApiService {

    String BASE_URL = "https://gateway.marvel.com:443";
    String DATE_FORMAT = "yyyy-MM-dd'T'hh:mm:ssZ"; //2029-12-31T00:00:00-0500
    String PUBLIC_KEY = "d43b4fbf1a63bca532a8bc7b660b3e40"/*BuildConfig.PUBLIC_API_KEY*/;
    String PRIVATE_KEY = "b50217071156dcf1aa12c298e441a2d8e48cb549" /*BuildConfig.PRIVATE_API_KEY*/;
    int DEFAULT_LIMIT = 100;

    /**
     * Fetches lists of comics with filters.
     *
     * @param limit     - limit of the responses to return
     * @param timeStamp - timestamp of the request
     * @param apiKey    - public api key
     * @param hash      - hash of the timestamp+private_key+public_key
     * @return an observable of {@link ComicDataResponse} containing a list of comics matching the
     * filters
     */
    @GET("/v1/public/characters")
    Observable<ComicDataResponse> getComics(@Query("limit") long limit,
                                            @Query("ts") String timeStamp,
                                            @Query("apikey") String apiKey,
                                            @Query("hash") String hash);

    /**
     * This method fetches a single comic resource.
     *
     * @param comicId   - id of the comic resource to fetch
     * @param timeStamp - timestamp of the request
     * @param apiKey    - public api key
     * @param hash      - hash of the timestamp+public_key+private_key
     * @return an observable of {@link ComicDataResponse} containing a single comic item
     */
    @GET("/v1/public/characters/{comicId}")
    Observable<ComicDataResponse> getComic(@Path("comicId") long comicId,
                                           @Query("ts") String timeStamp,
                                           @Query("apikey") String apiKey,
                                           @Query("hash") String hash);
}
