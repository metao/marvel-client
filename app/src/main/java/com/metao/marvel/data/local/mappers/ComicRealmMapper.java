package com.metao.marvel.data.local.mappers;


import com.metao.marvel.data.local.realm.ComicRealm;
import com.metao.marvel.data.model.Comic;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Maps from {@link ComicRealm} to {@link Comic}.
 */
@Singleton
public class ComicRealmMapper implements Mapper<ComicRealm, Comic> {

    private final ImageRealmMapper imageRealmMapper;

    /**
     * Create new comic realm mapper - it depends on the following mappers.
     *
     * @param imageRealmMapper - image realm mapper
     */
    @Inject
    public ComicRealmMapper(ImageRealmMapper imageRealmMapper) {
        this.imageRealmMapper = imageRealmMapper;
    }

    @Override
    public Comic map1(ComicRealm from) {
        return new Comic.Builder()
                .setId(from.getId())
                .setTitle(from.getTitle())
                .setDescription(from.getDescription())
                .setPageCount(from.getPageCount())
                .setThumbnail(imageRealmMapper.map1(from.getThumbnail()))
                .setImages(new ArrayList<>(imageRealmMapper.mapMany(from.getImages())))
                .build();
    }

    @Override
    public Collection<Comic> mapMany(Collection<ComicRealm> fromCollection) {
        List<Comic> comicList = new ArrayList<>();

        for (ComicRealm comicRealm : fromCollection) {
            comicList.add(map1(comicRealm));
        }

        return comicList;
    }
}
