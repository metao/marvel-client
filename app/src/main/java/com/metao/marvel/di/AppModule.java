package com.metao.marvel.di;


import android.content.Context;

import com.metao.marvel.App;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * App module defining how to provide app context.
 */
@Module
public class AppModule {

    private final App app;

    public AppModule(App app) {
        this.app = app;
    }

    @Provides
    @Singleton
    App provideApplication() {
        return app;
    }

    @Singleton
    @Provides
    Context provideContext() {
        return app.getApplicationContext();
    }

}
